<?php
/**
 * @file
 * This is the template file to generate the iframe based on the settings
 * determined by the video filter and site specific configuration from
 * feedroom_codec.
 */
?>

<iframe frameborder="<?php print $params['frameborder']; ?>"
        height="<?php print $params['height']; ?>"
        width="<?php print $params['width']; ?>"
        marginheight="<?php print $params['marginheight']; ?>"
        marginwidth="<?php print $params['marginwidth']; ?>"
        scrolling="<?php print $params['scrolling']; ?>"
        src="<?php print $video['source']; ?>">
</iframe>
