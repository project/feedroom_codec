<?php

/**
 * @file
 * Implementation of the administration pages of the module.
 */

/**
 * Form builder.
 *
 * @ingroup forms
 * @see system_settings_form()
 */
function feedroom_codec_settings_form() {
  $form = array();
  if (isset($_POST) && $_POST['form_id'] == 'feedroom_codec_settings_form') {
    $form += array(
      '#post' => $_POST,
      '#programmed' => FALSE,
    );
  }
  $form['feedroom_codec_site'] = array(
    '#type' => 'textfield',
    '#title' => t('The Feedroom public site URL'),
    '#maxlength' => 255,
    '#required' => TRUE,
    '#default_value' => variable_get('feedroom_codec_site', '//yoursite.feedroom.com'),
    '#description' => t('The URL for the feedroom site where your video content is hosted.'),
  );
  $form['feedroom_codec_display_mode'] = array(
    '#type' => 'radios',
    '#title' => t('Choose the display mode'),
    '#options' => array('iframe' => 'iframe', 'embed' => 'embed'),
    '#required' => TRUE,
    '#default_value' => variable_get('feedroom_codec_display_mode', 'iframe'),
    '#description' => t('Choose whether to output the video as an iframe or using an embed object.'),
    '#ahah' => array(
      'path' => 'feedroom-codec/ahah/switch-form-mode',
      'wrapper' => 'mode-dependent-fields',
      'effect' => 'fade',
    ),
  );

  $form['mode'] = array(
    '#type' => 'fieldset',
    '#title' => 'Display mode configuration options',
    '#prefix' => '<div id="mode-dependent-fields">',
    '#suffix' => '</div>',
  );

  if (isset($form['#post']['feedroom_codec_display_mode'])) {
    $mode = $form['#post']['feedroom_codec_display_mode'];
  }
  $mode = isset($mode) ? $mode : variable_get('feedroom_codec_display_mode', 'iframe');
  
  feedroom_generate_mode_dependent_fields($mode, $form, $edit);
  
  $form['advanced'] = array(
    '#type' => 'fieldset',
    '#title' => t('Advanced layout settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['advanced']['feedroom_codec_content_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Default width for your feedroom content'),
    '#maxlength' => 4,
    '#default_value' => variable_get('feedroom_codec_content_width', '595'),
    '#description' => t('The default width to use when displaying content from your feedroom site.'),
  );
  $form['advanced']['feedroom_codec_content_height'] = array(
    '#type' => 'textfield',
    '#title' => t('Default height for your feedroom content'),
    '#maxlength' => 4,
    '#default_value' => variable_get('feedroom_codec_content_height', '363'),
    '#description' => t('The default height to use when displaying content from your feedroom site.'),
  );
  $form['advanced']['feedroom_codec_margin_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Margin width'),
    '#maxlength' => 2,
    '#default_value' => variable_get('feedroom_codec_margin_width', '0'),
    '#description' => t('Width for the margin around the video content.'),
  );
  $form['advanced']['feedroom_codec_margin_height'] = array(
    '#type' => 'textfield',
    '#title' => t('Margin height'),
    '#maxlength' => 2,
    '#default_value' => variable_get('feedroom_codec_margin_height', '0'),
    '#description' => t('Height for the margin around the video content.'),
  );
  $form['advanced']['feedroom_codec_frame_border'] = array(
    '#type' => 'textfield',
    '#title' => t('Frame border'),
    '#maxlength' => 2,
    '#default_value' => variable_get('feedroom_codec_frame_border', '0'),
    '#description' => t('Thickness of the border around the video context in pixels.'),
  );
  $form['advanced']['feedroom_codec_scrolling'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable scrolling'),
    '#maxlength' => 2,
    '#default_value' => variable_get('feedroom_codec_scrolling', '0'),
    '#description' => t('Enable scrolling if your video content is bigger than the space available.'),
  );
  
  return system_settings_form($form);
}

/**
 * Generate settings fields for submodules (either barrier or wallpage ones).
 *
 * @param  $mode
 *  The display mode that feedroom is currently in.
 * @param  $form
 *  Form to add the new fields to
 * @param  $edit
 *  Values for the Form
 */
function feedroom_generate_mode_dependent_fields($mode, &$form, $edit) {
  $form['mode']['#title'] = t('!mode configuration options', array('!mode' => $mode));
  if ($mode == 'iframe') {
    $form['mode']['feedroom_codec_iframe_base_url'] = array(
      '#type' => 'textfield',
      '#title' => t('The base URL used by feedroom to link'),
      '#maxlength' => 255,
      '#required' => TRUE,
      '#default_value' => variable_get('feedroom_codec_iframe_base_url', 'http://yoursite.feedroom.com/linking/index.jsp'),
      '#description' => t('The base url used by your feedroom site when generating the iframe embed code.'),
    );
    $form['mode']['feedroom_codec_iframe_skin'] = array(
      '#type' => 'textfield',
      '#title' => t('The skin to use for displaying video content'),
      '#maxlength' => 255,
      '#default_value' => variable_get('feedroom_codec_iframe_skin', 'oneclip'),
      '#description' => t('The skin determines how the video is presented.'),
    );
  }
  else {
    $form['mode']['feedroom_codec_embed_base_url'] = array(
      '#type' => 'textfield',
      '#title' => t('The base URL used by feedroom when creating embed objects'),
      '#maxlength' => 255,
      '#required' => TRUE,
      '#default_value' => variable_get('feedroom_codec_embed_base_url', '//mysite.pb.feedroom.com/pb-comp/mysite/custom2/player.swf'),
      '#description' => t('The base url used by your feedroom site when generating the embed code.'),
    );
    $form['mode']['feedroom_codec_site_id'] = array(
      '#type' => 'textfield',
      '#title' => t('The id that Feedroom uses for your site'),
      '#maxlength' => 255,
      '#default_value' => variable_get('feedroom_codec_site_id', 'mysite'),
      '#description' => t('This is the ID that would appear in the embed code if you grabbed the video directly from Feedroom.'),
    );
    $form['mode']['feedroom_codec_site_name'] = array(
      '#type' => 'textfield',
      '#title' => t('The site name as it would appear in the video'),
      '#maxlength' => 255,
      '#default_value' => variable_get('feedroom_codec_site_name', 'My Site'),
      '#description' => t('The site name as it should be displayed to the users.'),
    );
    $form['mode']['feedroom_codec_embed_skin'] = array(
      '#type' => 'textfield',
      '#title' => t('The skin to use for displaying video content'),
      '#maxlength' => 255,
      '#default_value' => variable_get('feedroom_codec_embed_skin', 'custom2'),
      '#description' => t('The skin determines how the video is presented.'),
    );
  }
}

/**
 * Helper function for the two AHAH callbacks for wallpage and barrier settings.
 *
 * @param  $settings_schema_field
 *  The field to store the settings into
 */
function feedroom_settings_ahah_callback() {
  $form_state = array('storage' => NULL, 'submitted' => FALSE, 'rebuild' => TRUE);
  $form_build_id = $_POST['form_build_id'];
  $form = form_get_cache($form_build_id, $form_state);
  
  $args = $form['#parameters'];
  $form_id = array_shift($args);
  $form_state['post'] = $form['#post'] = $_POST;

  // Enable the submit/validate handlers to determine whether AHAH-submittted.
  $form_state['ahah_submission'] = TRUE;
  $form['#programmed'] = $form['#redirect'] = FALSE;

  drupal_process_form($form_id, $form, $form_state);
  $form = drupal_rebuild_form($form_id, $form_state, $args, $form_build_id);
  $settings = $form['mode'];

  // Remove the prefix/suffix wrapper so we don't double it up.
  unset($settings['#prefix'], $settings['#suffix']);

  // Clear any unwanted messages about validation etc.
  drupal_get_messages();

  // Render the output.
  $output .= drupal_render($settings);

  // Final rendering callback.
  drupal_json(array('status' => TRUE, 'data' => $output));
}